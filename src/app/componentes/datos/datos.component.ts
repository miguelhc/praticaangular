import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.css']
})
export class DatosComponent implements OnInit {

  datos: any = new Object();
  constructor() {
    this.datos.nombre = 'Eymie';
    this.datos.email = 'email';
    this.datos.direccion = 'av peru';
    this.datos.telefono = '963...';
   }

  ngOnInit() {
  }

}
