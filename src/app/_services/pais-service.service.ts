import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaisServiceService {

  domain = 'https://restcountries.eu/rest/v2/all';

  constructor(private http: HttpClient) { }

  getPaises() {
    return this.http.get<any>(this.domain)
    .pipe(map(data => data));
  }
}
