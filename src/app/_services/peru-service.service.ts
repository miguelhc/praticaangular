import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeruServiceService {

  domain = 'https://restcountries.eu/rest/v2/name/peru?fullText=true';

  constructor(private http: HttpClient) { }

  getPeru() {
    return this.http.get<any>(this.domain)
    .pipe(map(data => data));
  }
}
